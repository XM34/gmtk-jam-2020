tool
extends HBoxContainer

const MAXIMUM_CONTROL = 42

export(int) var value = 0 setget set_value, get_value

signal _on_zero_control
signal _on_max_control

func set_value(val):
	value = val
	if has_node("ControlLabel"):
		$ControlLabel.text = str(val)
	if value <= 0:
		value = 0
		emit_signal("_on_zero_control")
	if value > 42:
		emit_signal("_on_max_control")

func get_value():
	return value
