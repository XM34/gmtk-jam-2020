extends Control

export(Texture) var icon = preload("res://textures/ui/dungeon-gate.png") setget set_button_icon
export(bool) var disabled = false setget set_disabled
export(bool) var active = false setget set_active

var items = []

signal _on_activate


func set_button_icon(value):
	icon = value
	if has_node("Icon"):
		$Icon.texture = value


func set_disabled(value: bool):
	disabled = value
	if has_node("ButtonNormal"):
		$ButtonNormal.disabled = value
		$ButtonActive.disabled = value


func set_active(value:bool):
	active = value
	if not has_node("ButtonNormal"):
		return
	if value:
		$ButtonNormal.hide()
		$ButtonActive.show()
	else:
		$ButtonNormal.show()
		$ButtonActive.hide()

func _on_ButtonNormal_pressed():
	if not Engine.editor_hint:
		set_active(true)
		emit_signal("_on_activate")


func _on_ButtonActive_pressed():
	if not Engine.editor_hint:
		set_active(false)


func _on_mouse_entered():
	emit_signal("mouse_entered")


func _on_mouse_exited():
	emit_signal("mouse_exited")
