extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	$DungeonGenerator.generate_dungeon()
	yield(get_tree().create_timer(1), "timeout")
	
	$DungeonMap.fill_rect($DungeonGenerator.get_bounding_rect(), 1)
	print($DungeonGenerator.get_bounding_rect())
	
	#$DungeonMap.carve_line(Vector2(0, 0), Vector2(500, 500), 0)
	carve_dungeon()

func carve_dungeon():
	# Carve rooms
	for room in $DungeonGenerator/Rooms.get_children():
		$DungeonMap.fill_rect(room.get_rect(), 0)
	
	# Carve corridors
	if $DungeonGenerator.paths:
		var paths = ($DungeonGenerator.paths as AStar2D)
		var visited_points = []
		
		for point in paths.get_points():
			for target_point in paths.get_point_connections(point):
				if (target_point in visited_points):
					continue
				$DungeonMap.carve_line(
					paths.get_point_position(point),
					paths.get_point_position(target_point),
					0
				)
			visited_points.append(point)
