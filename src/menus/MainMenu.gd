extends Node



func _on_NewGameButton_pressed():
	get_tree().change_scene("res://src/GameOverview.tscn")


func _on_ExitButton_pressed():
	get_tree().quit()


func _on_TextButton_pressed():
	$MarginContainer/MenuAligner/MenuAligner/CenterContainer/MainMenu.hide()
	$MarginContainer/MenuAligner/MenuAligner/CenterContainer/SettingsMenu.show()


func _on_ExitGameButton_pressed():
	get_tree().quit()


func _on_FullscreenButton_pressed():
	OS.window_fullscreen = not OS.window_fullscreen


func _on_MusicButton_pressed():
	get_tree().get_root().get_node("/root/MusicService").toggle_music()


func _on_SettingsBackButton_pressed():
	$MarginContainer/MenuAligner/MenuAligner/CenterContainer/SettingsMenu.hide()
	$MarginContainer/MenuAligner/MenuAligner/CenterContainer/MainMenu.show()
