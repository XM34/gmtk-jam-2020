
const ADVENTURER_CLASSES = ["Rogue", "Mage", "Paladin", "Cleric", "Bard", "Warrior"]

var adv_name = ""
var adv_class = ""
var portrait = ""
var base_health = 12
var base_energy = 12
var description = ""


func _init(
	adv_name: String,
	adv_class: String,
	portrait: Texture,
	base_health: int,
	base_energy: int,
	description: String
):
	self.adv_name = adv_name
	self.adv_class = adv_class
	self.portrait = portrait
	self.base_health = base_health
	self.base_energy = base_energy
	self.description = description


const FILENAME = "res://data/adventurers.json"

static func get_all():
	var results = []
	var adventurer_data = load_json_data(FILENAME)
	
	if typeof(adventurer_data) == TYPE_ARRAY:
		for adventurer in adventurer_data:
			results.append(load("res://src/adventurers/Adventurer.gd").new(
				adventurer["name"],
				adventurer["class"],
				load(adventurer["portrait"]),
				adventurer["health"],
				adventurer["energy"],
				adventurer["description"]
			))
	
	return results


static func get_classes():
	var results = []
	var adventurer_data = load_json_data(FILENAME)
	
	if typeof(adventurer_data) == TYPE_ARRAY:
		for adventurer in adventurer_data:
			var adv_class = adventurer["class"]
			
			if not adv_class in results:
				results.append(adv_class)
	return results

static func load_json_data(filename):
	var file = File.new()
	file.open(filename, File.READ)
	var itemdata_json = JSON.parse(file.get_as_text())
	file.close()
	return itemdata_json.result
