extends Node2D

const ROOM_SCN = preload("res://src/dungeon/Room.tscn")
const BOUNDING_RIM = Vector2(8, 8)

export(bool) var DEBUG_DRAW = false

export(int) var tileSize = 32
export(int) var numRooms = 50
export(int) var minSize = 4
export(int) var maxSize = 10
export(Vector2) var spread = Vector2(0, 0)
export(float) var roomSpreading = 1.2

var paths = null

signal _on_generation_finished(paths)

func _ready():
	randomize()

func generate_dungeon():
	make_rooms()
	
	# Wait for physics
	yield(get_tree().create_timer(0.5), "timeout")
	
	make_paths()
	
	# Make rooms static
	for room in $Rooms.get_children():
		room.mode = RigidBody2D.MODE_STATIC
		room.position = room.position * roomSpreading
		(paths as AStar2D).set_point_position(room.id, room.position)
	
	emit_signal("_on_generation_finished", paths)

func make_rooms():
	for i in range(numRooms):
		var pos = Vector2(
			rand_range(-spread.x, spread.x),
			rand_range(-spread.y, spread.y)
		)
		var room = ROOM_SCN.instance()
		var roomWidth = minSize + randi() % (maxSize - minSize)
		var roomHeight = minSize + randi() % (maxSize - minSize)
		room.make_room(pos, Vector2(roomWidth, roomHeight) * tileSize)
		room.name = str("Room ", i)
		$Rooms.add_child(room)
	
	

# Create paths between neighbouring rooms
func make_paths():
	paths = AStar2D.new()
	for room in $Rooms.get_children():
		room.id = paths.get_available_point_id()
		paths.add_point(room.id, room.position)
	
	for room in $Rooms.get_children():
		for neighbour in room.get_neighbours():
			paths.connect_points(room.id, neighbour.id)

func _draw():
	if (!DEBUG_DRAW):
		return
	
	for room in $Rooms.get_children():
		draw_rect(
			Rect2(room.position - room.size, room.size * 2),
			Color(32, 228, 0),
			false
		)
	
	if paths:
		for p in paths.get_points():
			for c in paths.get_point_connections(p):
				draw_line(paths.get_point_position(p), paths.get_point_position(c), Color(1, 1, 0), true)

func get_bounding_rect():
	var bounding_rect = Rect2(0, 0, 1, 1)
	for room in $Rooms.get_children():
		var room_rect = Rect2(room.position - room.size - (BOUNDING_RIM * tileSize * 2),
			(room.size * 2) + (BOUNDING_RIM * tileSize * 4))
		bounding_rect = bounding_rect.merge(room_rect)
	return bounding_rect

func _process(delta):
	update()
