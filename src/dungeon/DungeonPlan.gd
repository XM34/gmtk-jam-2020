extends Node2D

const LINE_TEXTURE = preload("res://textures/ui/corridor-line.png")
const LINE_TEXTURE_ACTIVE = preload("res://textures/ui/corridor-line-active.png")
const LINE_TEXTURE_INACTIVE = preload("res://textures/ui/corridor-line-inactive.png")

const ROOM_SCN = preload("res://src/menus/RoomButton.tscn")
const ROOM_TYPE = preload("res://src/menus/RoomButton.gd")

signal room_entered(room)
signal _on_dungeon_generated(rooms)

func _ready():
	plan_dungeon()

func plan_dungeon():
	# Generate dungeon layout
	$DungeonGenerator.generate_dungeon()
	yield(get_tree().create_timer(1), "timeout")
	
	# Draw paths
	if $DungeonGenerator.paths:
		var paths = ($DungeonGenerator.paths as AStar2D)
		var visited_points = []
		
		for point in paths.get_points():
			for target_point in paths.get_point_connections(point):
				if (target_point in visited_points):
					continue
				draw_path(
					str(point, ",", target_point),
					paths.get_point_position(point),
					paths.get_point_position(target_point)
				)
			visited_points.append(point)
	
	# Draw rooms
	for room in $DungeonGenerator/Rooms.get_children():
		draw_room(room.id, room.position)
	
	var activated_room = ($Rooms.get_child(0) as ROOM_TYPE)
	activate_room(activated_room)
	$PlayerCamera.position = activated_room.get_position()
	
	emit_signal("_on_dungeon_generated", $Rooms.get_children())

func draw_path(id, from, to):
	var line = Line2D.new()
	line.name = id
	line.texture = LINE_TEXTURE_INACTIVE
	line.texture_mode = Line2D.LINE_TEXTURE_TILE
	line.add_point(from)
	line.add_point(to)
	$Paths.add_child(line)

func draw_room(id, pos):
	var room = ROOM_SCN.instance()
	room.name = str(id)
	room.set_position(pos)
	room.disabled = true
	$Rooms.add_child(room)
	room.connect("_on_activate", self, "_on_activate_room", [room])

func activate_room(activated_room):
	var room_id = int(activated_room.name)
	
	activated_room.active = true
	activated_room.disabled = true
	
	var connections = ($DungeonGenerator.paths as AStar2D).get_point_connections(room_id)
	for room in $Rooms.get_children():
		var curr_room_id = int(room.name)
		if int(room.name) in connections:
			# Enable connected rooms that are not active
			if not room.active:
				room.disabled = false
			# Enable paths
			for path in $Paths.get_children():
				if path.name == str(room.name, ",", room_id) or path.name == str(room_id, ",", room.name):
					if room.active:
						path.texture = LINE_TEXTURE_ACTIVE
					else:
						path.texture = LINE_TEXTURE
	# Move party to new room
	$PlayerMarker.position = activated_room.get_position()

func _on_activate_room(room):
	activate_room(room)
	emit_signal("room_entered", room)
