extends RigidBody2D

var size
var id

func make_room(_pos, _size, _contactSize = 10):
	position = _pos
	size = _size
	
	var shape = RectangleShape2D.new()
	shape.custom_solver_bias = 0.75
	shape.extents = size
	$RoomShape.shape = shape
	
	var contactShape = RectangleShape2D.new()
	contactShape.custom_solver_bias = 0.75
	contactShape.extents = size + Vector2(_contactSize, _contactSize)
	$RoomContactArea/RoomContactShape.shape = contactShape

func get_neighbours():
	var neighbours = $RoomContactArea.get_overlapping_bodies()
	neighbours.erase(self)
	return neighbours

func get_rect():
	return Rect2(position - size, size * 2)

func _to_string():
	return name
