extends TileMap

func _ready():
	randomize()

func fill_rect(rect: Rect2, tile: int):
	var rect_start = world_to_map(rect.position)
	var rect_end = world_to_map(rect.position + rect.size)
	
	for x in range(rect_start.x, rect_end.x):
		for y in range(rect_start.y, rect_end.y):
			set_cell(x, y, tile)

func carve_line(start: Vector2, end: Vector2, tile: int):
	var start_tile = world_to_map(start)
	var end_tile = world_to_map(end) 
	
	get_tree().create_timer(0.5)
	
	if (start.x == end.x):
		# Draw vertical line
		var range_start = min(end_tile.y, start_tile.y)
		var range_end = max(start_tile.y, end_tile.y)
		for y in range(range_start, range_end):
			set_cell(start_tile.x, y, tile)
		set_cell(start_tile.x, range_end, tile)
	elif (start.y == end.y):
		# Draw horizontal line
		var range_start = min(end_tile.x, start_tile.x)
		var range_end = max(start_tile.x, end_tile.x)
		for x in range(range_start, range_end):
			set_cell(x, start_tile.y, tile)
		set_cell(range_end, start_tile.y, tile)
	else:
		if(randi() % 2 == 0):
			# Move vertically first
			carve_line(start, Vector2(start.x, end.y), tile)
			carve_line(Vector2(start.x, end.y), end, tile)
		else:
			# Move horizontally first
			carve_line(start, Vector2(end.x, start.y), tile)
			carve_line(Vector2(end.x, start.y), end, tile)
