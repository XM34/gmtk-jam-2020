extends Node

const ROGUE_SCN = preload("res://src/adventurers/Rogue.tscn")
const MAGE_SCN = preload("res://src/adventurers/Mage.tscn")
const PALADIN_SCN = preload("res://src/adventurers/Paladin.tscn")
const CLERIC_SCN = preload("res://src/adventurers/Cleric.tscn")
const BARD_SCN = preload("res://src/adventurers/Bard.tscn")
const WARRIOR_SCN = preload("res://src/adventurers/Warrior.tscn")

func generate_party(size: int):
	randomize()
	var adventurer_list = get_adventurer_list()
	var party = []
	
	for _i in range(size):
		var next_adv_idx = randi() % len(adventurer_list)
		var next_adventurer = adventurer_list[next_adv_idx].instance()
		party.append(next_adventurer)
		adventurer_list.remove(next_adv_idx)
	
	return party

func get_adventurer_list():
	return [
		ROGUE_SCN,
		MAGE_SCN,
		PALADIN_SCN,
		CLERIC_SCN,
		BARD_SCN,
		WARRIOR_SCN
	]

func load_json_data(filename):
	var file = File.new()
	file.open(filename, File.READ)
	var itemdata_json = JSON.parse(file.get_as_text())
	file.close()
	return itemdata_json.result
