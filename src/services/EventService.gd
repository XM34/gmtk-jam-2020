extends Node


const FILENAME = "res://data/events.json"
const MAX_ITEMS_PER_ROOM = 3

var rng = RandomNumberGenerator.new()
var item_data


func load_json_data(filename):
	var file = File.new()
	file.open(filename, File.READ)
	var itemdata_json = JSON.parse(file.get_as_text())
	file.close()
	return itemdata_json.result

func load_event(character: String, itemname: String):
	if item_data == null:
		item_data = load_json_data(FILENAME)
	
	var search_result_array = []
	if item_data.has(itemname):
		var item = item_data[itemname]
		if item.has(character):
			search_result_array.append(item[character])
		else:
			search_result_array.append(item["Default"])
	else:
		print("Zur Hölle hast du denn da gefunden?")
	
	# choose random item from search result array -> TODO in der suche noch umbauen
	var search_result = search_result_array[randi() % search_result_array.size()]
	
	# choose random event from result
	var search_keys = search_result.keys()
	var search_weights = []
	for key in search_keys:
		search_weights.append(search_result[key]["Weight"])
	
	# sum over all weights
	var random_index = 0
	var sum = 0.0
	for element in search_weights:
		sum += int(element)
	var random_number = rng.randf_range(0,sum)
	for index in range(search_weights.size()):
		if search_weights[index] < random_number:
			random_number = random_number-search_weights[index]
		else:
			random_index = index
			break
	var random_key = search_keys[random_index]
	#var random_key = search_keys[randi() % search_keys.size()]
	var result_event = search_result[random_key]
	result_event["Message"].replace("__classname",character)
	return result_event

func generate_room():
	if item_data == null:
		item_data = load_json_data(FILENAME)
	
	var items = []
	for item in item_data:
		items.append(item)
	
	# Chose up to n items for the room
	var result = []
	
	for _i in (randi() % (MAX_ITEMS_PER_ROOM-1)) + 1:
		var random_item = chose_random_element(items)
		if not random_item in result:
			result.append(random_item)
	
	return result

func chose_random_element(list: Array):
	return list[randi() % len(list)]
