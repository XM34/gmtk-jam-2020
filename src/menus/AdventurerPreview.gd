tool
extends PanelContainer

const ADVENTURER = preload("res://src/adventurers/Adventurer.gd")

export(String) var adv_name setget set_adv_name
export(String) var adv_class setget set_adv_class
export(Texture) var portrait setget set_portrait
export(int) var base_health setget set_base_health
export(int) var base_energy setget set_base_energy
export(String) var description setget set_description


func set_adv_name(value):
	adv_name = value
	if has_node("Margin/VBox/Header/Info/Name"):
		$Margin/VBox/Header/Info/Name.text = value

func set_adv_class(value):
	adv_class = value
	if has_node("Margin/VBox/Header/Info/Class"):
		$Margin/VBox/Header/Info/Class.text = str("(", value, ")")

func set_portrait(value):
	portrait = value
	if has_node("Margin/VBox/BasicStats/Portrait"):
		$Margin/VBox/BasicStats/Portrait.texture = value

func set_base_health(value):
	base_health = value
	if has_node("Margin/VBox/BasicStats/Center/Stats/Health"):
		$Margin/VBox/BasicStats/Center/Stats/Health.text = str(value)

func set_base_energy(value):
	base_energy = value
	if has_node("Margin/VBox/BasicStats/Center/Stats/Energy"):
		$Margin/VBox/BasicStats/Center/Stats/Energy.text = str(value)

func set_description(value):
	description = value
	if has_node("Margin/VBox/Description"):
		$Margin/VBox/Description.text = value


func load_adventurer(adventurer: ADVENTURER):
	set_adv_name(adventurer.adv_name)
	set_adv_class(adventurer.adv_class)
	set_portrait(adventurer.portrait)
	set_base_health(adventurer.base_health)
	set_base_energy(adventurer.base_energy)
	set_description(adventurer.description)
