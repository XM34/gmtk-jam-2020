extends PanelContainer

const EVENT = preload("res://src/menus/Event.tscn")

func print_event_log(message):
	var event = EVENT.instance()
	event.text = message
	$Margin/EventScroller/EventList.add_child(event)
	yield(get_tree(), "idle_frame")
	$Margin/EventScroller.scroll_vertical = $Margin/EventScroller.get_v_scrollbar().max_value

func print_important_log(message):
	var event = EVENT.instance()
	event.text = message
	event.add_color_override("font_color", Color(43, 2, 2))
	$Margin/EventScroller/EventList.add_child(event)
	yield(get_tree(), "idle_frame")
	$Margin/EventScroller.scroll_vertical = $Margin/EventScroller.get_v_scrollbar().max_value
