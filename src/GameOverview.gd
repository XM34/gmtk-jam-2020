extends Node

const ADVENTURER = preload("res://src/adventurers/Adventurer.gd")
const ADVENTURER_INSTANCE_SCN = preload("res://src/adventurers/AdventurerInstance.tscn")
const ROOM_SCN = preload("res://src/menus/RoomButton.gd")
const END_SCREEN_SCN = preload("res://src/menus/EndScreen.tscn")
const WIN_SCREEN_SCN = preload("res://src/menus/WinScreen.tscn")


func _ready():
	generate_adventurers()


func _input(event):
	if event.is_action_pressed("open_menu"):
		get_tree().change_scene("res://src/menus/MainMenu.tscn")


func _on_DungeonPlan_room_entered(room):
	# Trigger events
	for item in room.items:
		if len(get_living_adventurers()) == 0:
			return
		
		var adventurer = get_random_living_adventurer()
		var triggered_event = $"/root/EventService".load_event(adventurer.adv_class, item)
		triggered_event["Message"] = str(triggered_event["Message"]).replace("__classname", adventurer.adv_class)
		$UiLayer/UI/Messages/EventLog.print_event_log(triggered_event["Message"])
		handle_adventurer_event(triggered_event, adventurer)
		handle_control_event(triggered_event)
	
	$UiLayer/UI/ControlMargin/ControlCounter.value -= 1


func _on_zero_control():
	# Todo lose game
	$UiLayer/UI/Messages/EventLog.print_important_log("Out of control, you lost!")
	lose("Out of Control!")

func _on_max_control():
	$UiLayer/UI/Messages/EventLog.print_important_log("Maximum Control reched, you won!")
	win("Maximum Control reached!")


func _on_dungeon_generated(rooms):
	for room in rooms:
		# Generate room content
		var room_items = $"/root/EventService".generate_room()
		room.items = room_items
		
		# Add tooltip handler
		(room as ROOM_SCN).connect("mouse_entered", self, "display_tooltip", [room])
		(room as ROOM_SCN).connect("mouse_exited", $UiLayer/TooltipFactory, "hide_tooltip")


func _on_PartyPreview_reroll_party():
	for i in range($UiLayer/UI/Heros/HBox.get_child_count()):
		$UiLayer/UI/Heros/HBox.get_child(i).queue_free()
	
	generate_adventurers()


func _on_press_start():
	$UiLayer/UI.show()
	$DungeonPlan.show()


func generate_adventurers():
	var party = generate_party(3)
	for adventurer in party:
		# Display adventurer in pre game scree
		$UiLayer/PartyPreview.add_adventurer(adventurer)
		
		# Display adventurer at the bottom of the game screen
		var adv_instance = ADVENTURER_INSTANCE_SCN.instance()
		$UiLayer/UI/Heros/HBox.add_child(adv_instance)
		adv_instance.load_adventurer(adventurer)
		
		# Debug log
		print(adventurer.adv_class, " Health:", adventurer.base_health)
		print(adventurer.adv_class, " Energy:", adventurer.base_energy)

func generate_party(size: int):
	var adventurer_list = ADVENTURER.get_all()
	var party = []
	
	for _i in range(size):
		var next_adv_idx = randi() % len(adventurer_list)
		var next_adventurer = adventurer_list[next_adv_idx]
		party.append(next_adventurer)
		adventurer_list.remove(next_adv_idx)
	
	return party


func get_random_living_adventurer():
	var living_adventurers = get_living_adventurers()
	
	var adv_idx = randi() % len(living_adventurers)
	return living_adventurers[adv_idx]


func get_living_adventurers():
	var living_adventurers = []
	for adventurer in $UiLayer/UI/Heros/HBox.get_children():
		if adventurer.health > 0 and adventurer.energy > 0:
			living_adventurers.append(adventurer)
	return living_adventurers


func handle_adventurer_event(event, adventurer):
	if event["Effects"].has("Health"):
		adventurer.health = clamp(adventurer.health + int(event["Effects"]["Health"]), 0, adventurer.max_health)
		print (adventurer.adv_class, " health updated (", event["Effects"]["Health"], ") now ", adventurer.health)
		if adventurer.health <= 0:
			adventurer.health = 0
			$UiLayer/UI/Messages/EventLog.print_important_log(str("The ", adventurer.adv_class, " dies!"))
			if len(get_living_adventurers()) == 0:
				lose("Total Party Kill!")
	if event["Effects"].has("Energy"):
		adventurer.energy = clamp(adventurer.energy + int(event["Effects"]["Energy"]), 0, adventurer.max_energy)
		print (adventurer.adv_class, " energy updated (", event["Effects"]["Energy"], ") now ", adventurer.energy)
		if adventurer.energy <= 0:
			adventurer.energy = 0
			$UiLayer/UI/Messages/EventLog.print_important_log(str("The ", adventurer.adv_class, " falls asleep and can't continue!"))
			if len(get_living_adventurers()) == 0:
				lose("Total Party Kill!")


func handle_control_event(event):
	if event["Effects"].has("Control"):
		$UiLayer/UI/ControlMargin/ControlCounter.value += int(event["Effects"]["Control"])


func display_tooltip(room: ROOM_SCN):
	$UiLayer/TooltipFactory.show_tooltip("Dungeon Room", print_items(room.items))


func print_items(items):
	var result = ""
	for item in items:
		result = str(result, "- ", item, "\n")
	return result


func lose(text):
	var end_screen = END_SCREEN_SCN.instance()
	end_screen.main_text = text
	$UiLayer.add_child(end_screen)


func win(text):
	var win_screen = WIN_SCREEN_SCN.instance()
	win_screen.main_text = text
	$UiLayer.add_child(win_screen)
