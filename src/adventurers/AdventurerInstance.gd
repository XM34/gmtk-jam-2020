tool
extends Control

const ADVENTURER = preload("res://src/adventurers/Adventurer.gd")

export(Texture) var portrait setget set_portrait
export(String) var adv_name = "Adventurer" setget set_name
export(String, "Rogue", "Mage", "Paladin", "Cleric", "Bard", "Warrior") var adv_class = "Rogue" setget set_class
export(int) var max_health = 12 setget set_max_health
export(int) var max_energy = 12 setget set_max_energy
export(int) var health = 12 setget set_health
export(int) var energy = 12 setget set_energy

func _ready():
	
	pass

func set_portrait(texture):
	portrait = texture
	if has_node("Margin/VBox/Body/Portrait"):
		$Margin/VBox/Body/Portrait.texture = texture

func set_name(name):
	adv_name = name
	if has_node("Margin/VBox/Header/Info/Name"):
		$Margin/VBox/Header/Info/Name.text = name

func set_class(adventurer_class):
	adv_class = adventurer_class
	if has_node("Margin/VBox/Header/Info/Class"):
		$Margin/VBox/Header/Info/Class.text = "(" + adv_class + ")"

func set_health(value):
	health = clamp(value, 0, max_health)
	if has_node("Margin/VBox/Body/Status/Health"):
		$Margin/VBox/Body/Status/Health.value = value

func set_energy(value):
	energy = clamp(value, 0, max_energy)
	if has_node("Margin/VBox/Body/Status/Energy"):
		$Margin/VBox/Body/Status/Energy.value = value

func set_max_health(value):
	max_health = value
	if has_node("Margin/VBox/Body/Status/Health"):
		$Margin/VBox/Body/Status/Health.max_value = value

func set_max_energy(value):
	max_energy = value
	if has_node("Margin/VBox/Body/Status/Energy"):
		$Margin/VBox/Body/Status/Energy.max_value = value

func load_adventurer(adventurer: ADVENTURER):
	set_name(adventurer.adv_name)
	set_class(adventurer.adv_class)
	set_portrait(adventurer.portrait)
	
	set_max_health(adventurer.base_health)
	set_health(adventurer.base_health)
	set_max_energy(adventurer.base_energy)
	set_energy(adventurer.base_energy)
