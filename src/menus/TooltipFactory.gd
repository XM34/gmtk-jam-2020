extends Node

onready var tooltip = $Tooltip


func show_tooltip(title: String, text: String):
	if tooltip != null:
		tooltip.title = title
		tooltip.text = text
		tooltip.set_global_position(tooltip.get_global_mouse_position())
		tooltip.show()
	else:
		print("No tooltip found")

func hide_tooltip():
	if tooltip != null:
		tooltip.hide()
